package fit5042.tutex.calculator;

import java.util.HashSet;
import java.util.Set;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;

import fit5042.tutex.repository.entities.Property;

/**
 * A simple compare property implementation
 *
 * @author Xiaoxiao Yan
 */

@Remote
@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	
	private Set<Property> propertySet;
	
	public ComparePropertySessionBean() {
		propertySet = new HashSet<>();
	}

	@Override
	public void addProperty(Property property) {
		propertySet.add(property);	
	}

	@Override
	public void removeProperty(Property property) {
		for (Property property1 : propertySet){
			if (property1.getPropertyId() == property.getPropertyId()) {
				propertySet.remove(property);
				break;
			}
		}
	}

	@Override
	public int bestPerRoom() {
		int propertyIdNumber = 0;
		double priceOfPerRoom;
		double minPriceOfPerRoom = 0;
		for (Property property: propertySet) 
		{
			priceOfPerRoom = property.getPrice() / property.getNumberOfBedrooms();
			if (minPriceOfPerRoom == 0) {
				minPriceOfPerRoom = priceOfPerRoom;
				propertyIdNumber = property.getPropertyId();
			}
			else if (minPriceOfPerRoom >= priceOfPerRoom) 
			{
				minPriceOfPerRoom = priceOfPerRoom;
				propertyIdNumber = property.getPropertyId();
			}
		}
		return propertyIdNumber;
	}
	
}
