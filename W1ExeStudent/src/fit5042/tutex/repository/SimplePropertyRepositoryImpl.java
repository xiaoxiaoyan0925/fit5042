/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Xiaoxiao Yan
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository {
	
	private ArrayList<Property> properties;

    public SimplePropertyRepositoryImpl() {
        properties = new ArrayList<Property>();
    }

	@Override
	public void addProperty(Property property) throws Exception {
		boolean hasAdded = false;
		for (Property property1: properties) {
			if (property1.getId() == property.getId()) 
			    hasAdded = true;
		}
		if (hasAdded == false)
			properties.add(property);
		
	}

	@Override
	public Property searchPropertyById(int id) throws Exception {
		for (Property property: properties) {
			if (property.getId() == id) {
			    return property;
			}
		}
		return null;
	}

	@Override
	public ArrayList<Property> getAllProperties() throws Exception {
		// TODO Auto-generated method stub
		return properties;
	}

	public ArrayList<Property> getProperties() {
		return properties;
	}

	public void setProperties(ArrayList<Property> properties) {
		this.properties = properties;
	}
    
}
 