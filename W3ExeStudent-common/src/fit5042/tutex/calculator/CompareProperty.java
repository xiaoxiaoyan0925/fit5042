package fit5042.tutex.calculator;

import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import fit5042.tutex.repository.entities.*;

/**
 * A simple compare property implementation
 *
 * @author Xiaoxiao Yan
 */

@Remote
public interface CompareProperty {

	void addProperty(Property property);
	void removeProperty(Property property);
	int bestPerRoom();
}
